import { all, takeLatest, call, fork, put } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import io from 'socket.io-client';

import { setToken } from '../../utils/auth';
import { get } from '../../utils/fetch';

import { signIn } from '../../redux/modules/app/app';
import {getEmail} from 'utils/auth';
import {fetchInitialData} from 'redux/modules/game/game';


function* signInIterator({ payload }) {
  const { service, token } = payload;

  try {
    const data = yield call(get, `/user/auth/${service}/token?access_token=${token}`);
    yield put(signIn.success());
    yield call(setToken, data.accessToken);
    window.socket.close();
    window.socket = yield call(io.connect, 'https://game-api.secrettech.io/', { query: `token=${data.accessToken}` });
    yield put(push('/garage'));
    const response = yield call(callBackWrapper);
    const { players } = response;
    const player = players.filter((p) => p.email === getEmail())[0];
    yield put(fetchInitialData({ player, ...response }));
    yield put(push(`/game?trackId=${response.id}`));
  } catch (e) {
    console.log(e);
    yield put(signIn.failure());
  }
}

function callBackWrapper() {
  return new Promise(resolve => {
    window.socket.on('start', (data) => {
      resolve(data);
    });
  })
}

function* signInSaga() {
  yield takeLatest(
    signIn.REQUEST,
    signInIterator
  );
}


export default function* () {
  yield all([
    fork(signInSaga)
  ]);
}
